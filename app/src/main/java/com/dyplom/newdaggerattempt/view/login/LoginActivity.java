package com.dyplom.newdaggerattempt.view.login;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dyplom.newdaggerattempt.R;
import com.dyplom.newdaggerattempt.di.AppComponent;
import com.dyplom.newdaggerattempt.di.AppModule;
import com.dyplom.newdaggerattempt.di.login.DaggerLoginComponent;
import com.dyplom.newdaggerattempt.di.login.LoginComponent;
import com.dyplom.newdaggerattempt.di.login.LoginModule;
import com.dyplom.newdaggerattempt.model.user.User;
import com.dyplom.newdaggerattempt.util.NfcHelper;
import com.dyplom.newdaggerattempt.view.main.MainActivity;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.support.DaggerAppCompatActivity;
import retrofit2.Retrofit;

public class LoginActivity extends DaggerAppCompatActivity implements LoginInterface {

    //@Inject something from appropriative module
    private static final String TAG = "LoginActivity";

    private EditText loginEditText, passwordEditText;
    private Button loginAction;
    private LoginPresenter loginPresenter;
    private ProgressDialog progressDialog;

    private User user = null;

    private NfcAdapter nfcAdapter;
    private NfcHelper nfcHelper;
    private String tagServerPass = null;

    @Inject
    LoginApi loginApi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initializeDaggerLoginComponent();


        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcHelper = new NfcHelper(this);

        loginEditText = findViewById(R.id.main_login);
        passwordEditText = findViewById(R.id.main_password);
        loginAction = findViewById(R.id.main_action_login);

        loginPresenter = new LoginPresenter(this);

        loginAction.setOnClickListener(v -> {
            String loginText = loginEditText.getText().toString();
            String passwordText = passwordEditText.getText().toString();

            loginPresenter.signIn(loginText, passwordText);
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please, wait a monent...");

        Log.d(TAG, "onCreate: "+loginApi);

    }

    private void initializeDaggerLoginComponent() {
        LoginComponent loginComponent = DaggerLoginComponent.builder()
                .appModule(new AppModule())
                .loginModule(new LoginModule())
                .build();
    }


    @Override
    public void onSuccess(User user) {
        Toast.makeText(this, user.getEmail(), Toast.LENGTH_SHORT).show();
        this.user = user;
        if (user.getNfcIsOn() == "1" && !user.getServerPass().equals(tagServerPass)) {
            Toast.makeText(this, "Attach your tag please", Toast.LENGTH_SHORT).show();
            return;
        } else {
            Toast.makeText(this, "Go to the klient page", Toast.LENGTH_SHORT).show();
        }
        goToKlientActivity();
    }

    private void goToKlientActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onFailed(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Boolean nfcVerify(String serverPass) {

        //On this moment user is null, because we get his only in success. Now stage before onSuccess.

        Boolean result = false;

        Toast.makeText(this, "Server pass:" + serverPass, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "nfcVerify: tagServerPass: "+tagServerPass );
        if (tagServerPass == null) {
            Toast.makeText(this, "Attach a tag please", Toast.LENGTH_SHORT).show();
        }
        if (serverPass == null) {
            Log.d(TAG, "nfcVerify : serverpass is null");
        }
        if (tagServerPass == null) {
            Log.d(TAG, "nfcVerify: tagSeverPass is null");
        }
        if (tagServerPass != null && tagServerPass.equals(serverPass)) {
            result= true;
        }

        hideProgress();
        return result;
    }

    @Override
    public void setMessageFromNfcTag(String serverPass) {//Working
        if (tagServerPass == null) {
            tagServerPass = serverPass;
            Toast.makeText(this, "Tag is read" + tagServerPass, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Tag is read again" + tagServerPass, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        nfcHelper.readInOnNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableForegroundDispatchSystem();
    }

    @Override
    protected void onPause() {
        super.onPause();
        disableForegroundDispatchSystem();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        tagServerPass = null;
    }

    private void enableForegroundDispatchSystem() {
        Intent intent = new Intent(this, LoginActivity.class).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        IntentFilter[] intentFilters = new IntentFilter[]{};
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilters, null);
    }

    private void disableForegroundDispatchSystem() {
        nfcAdapter.disableForegroundDispatch(this);
    }

}
