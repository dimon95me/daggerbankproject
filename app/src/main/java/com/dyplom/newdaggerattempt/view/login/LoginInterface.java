package com.dyplom.newdaggerattempt.view.login;

import com.dyplom.newdaggerattempt.model.user.User;

public interface LoginInterface extends ShowDialogInterface {
    void onSuccess(User user);

    void onFailed(String error);

    Boolean nfcVerify(String serverPass);

    void setMessageFromNfcTag(String serverPass);
}
