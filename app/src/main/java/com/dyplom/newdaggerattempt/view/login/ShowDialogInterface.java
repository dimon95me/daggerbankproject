package com.dyplom.newdaggerattempt.view.login;

public interface ShowDialogInterface {
    void showProgress();
    void hideProgress();
}
