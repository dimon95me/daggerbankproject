package com.dyplom.newdaggerattempt.view.login;

import com.dyplom.newdaggerattempt.model.user.UserJsonShell;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LoginApi {
    @GET("login_response.php")
    Call<UserJsonShell> login(@Query("login_field") String login, @Query("password") String password);
}
