package com.dyplom.newdaggerattempt.di.login;

import com.dyplom.newdaggerattempt.di.AppModule;
import com.dyplom.newdaggerattempt.view.login.LoginActivity;

import dagger.Component;

@Component(modules = {LoginModule.class})
@LoginScope
public interface LoginComponent {
}
