package com.dyplom.newdaggerattempt.di.login;

import com.dyplom.newdaggerattempt.di.AppModule;
import com.dyplom.newdaggerattempt.view.login.LoginApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {AppModule.class})
public class LoginModule {

    @LoginScope
    @Provides
    static LoginApi provideLoginApi(Retrofit retrofit) {
        return retrofit.create(LoginApi.class);
    }

}
