package com.dyplom.newdaggerattempt.di;


import com.dyplom.newdaggerattempt.di.login.LoginScope;
import com.dyplom.newdaggerattempt.view.login.LoginActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuildersModule {

    @LoginScope
    @ContributesAndroidInjector
    abstract LoginActivity contributeLoginActivity();

}
