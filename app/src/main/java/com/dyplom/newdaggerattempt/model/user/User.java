package com.dyplom.newdaggerattempt.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

    @SerializedName("id_klent")
    @Expose
    private String idKlent;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("surname")
    @Expose
    private String surname;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("server_pass")
    @Expose
    private String serverPass;
    @SerializedName("nfc_is_on")
    @Expose
    private String nfcIsOn;
    @SerializedName("secret_question")
    @Expose
    private String secretQuestion;
    @SerializedName("secret_answer")
    @Expose
    private String secretAnswer;

}